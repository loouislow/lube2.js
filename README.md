# lube2.js

> Enhanced image lazy-load pico library without jQuery

Alright! This is another method to not-exactly-lazy-load the images, this is way better than the first lube.js. Lube2.js is incredibly smaller and it's way too easy to use.

## Usage

To add an image directly into the page simply add a `data-lube` attribute to the img tag.

Please note that `your-fake-image.png` can be put image that shows as preloader or by your imagination.

```html
<body>

    <img src="your-fake-image.png" data-lube="your-real-image.jpg">
    
    <script src="lube2.js"></script>

</body>
```

## Story

I was making another new UI design project, and somehow my hunch tells me the lube.js(one) can be enhanced and shaped a little more. I recently made a page that took eight seconds to load go down to less than a second using only this method.

I didn't explace at lube.js(one), so I can make it here in lube2.js. When the page is initially loaded, the browser will get the "fake image" once and then that will be the only image the browser sees, so whether you have one image or a hundred, it won't matter because the browser has already downloaded the fake image.

Then via javascript we swap out the fake image with the real one. When the browser sees that there is a new image in the html, it will now download it.

This relatively simple step can provide amazing results. Enjoy!
